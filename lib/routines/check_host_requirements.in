#!/bin/bash

# This file is part of s3tfpl utility, see LICENSE file for details
# Copyright (C) 2022 - The Pulsar Linux Team

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Title:        check_host_requirements
# Description:  Checks the host system software version requirements
# Author:       Oscar Campos <oscar.campos@pulsarlinux.org>
# Date:         2022-04-06 21:24:11
# Version:      0.0.1

# Exit codes
# ==========
# 0    no error
# 2    version check failure

set -o errexit # ensure we exit in case of errors
set -o nounset # ensure we exit if variables are not initialized
set +h         # disable hashall

function check_host_requirements() {
    : <<inline_doc
    Check if the running host has installed the required software versions
    to proceed with the Stage 3 build process

    returns:    nothing
    on error:   writes to the terminal if DEBUG and exists with 2
    on success: writes to the terminal if DEBUG
inline_doc

    echo "Checking host software requirements..."
    if [ -n "${Min_Bash_VER}" ]; then
        check_bash_version "${Min_Bash_VER}"
    fi

    if [ -n "${Min_Binutils_VER}" ]; then
        check_binutils_version "${Min_Binutils_VER}"
    fi

    if [ -n "${Min_Bison_VER}" ]; then
        check_bison_version "${Min_Bison_VER}"
    fi

    if [ -n "${Min_Coreutils_VER}" ]; then
        check_coreutils_version "${Min_Coreutils_VER}"
    fi

    if [ -n "${Min_Diffutils_VER}" ]; then
        check_diffutils_version "${Min_Diffutils_VER}"
    fi

    if [ -n "${Min_Findutils_VER}" ]; then
        check_findutils_version "${Min_Findutils_VER}"
    fi

    if [ -n "${Min_Gawk_VER}" ]; then
        check_gawk_version "${Min_Gawk_VER}"
    fi

    if [ -n "${Min_GCC_VER}" ]; then
        check_gcc_version "${Min_GCC_VER}" "g++"
    fi

    if [ -n "${Min_Grep_VER}" ]; then
        check_grep_version "${Min_Grep_VER}"
    fi

    if [ -n "${Min_Gzip_VER}" ]; then
        check_gzip_version "${Min_Gzip_VER}"
    fi

    if [ -n "${Min_Kernel_VER}" ]; then
        check_kernel_version "${Min_Kernel_VER}"
    fi

    if [ -n "${Min_M4_VER}" ]; then
        check_m4_version "${Min_M4_VER}"
    fi

    if [ -n "${Min_Make_VER}" ]; then
        check_make_version "${Min_Make_VER}"
    fi

    if [ -n "${Min_Patch_VER}" ]; then
        check_patch_version "${Min_Patch_VER}"
    fi

    if [ -n "${Min_Perl_VER}" ]; then
        check_perl_version "${Min_Perl_VER}"
    fi

    if [ -n "${Min_Python_VER}" ]; then
        check_python_version "${Min_Python_VER}"
    fi

    if [ -n "${Min_Sed_VER}" ]; then
        check_sed_version "${Min_Sed_VER}"
    fi

    if [ -n "${Min_Tar_VER}" ]; then
        check_tar_version "${Min_Tar_VER}"
    fi

    if [ -n "${Min_Texinfo_VER}" ]; then
        check_texinfo_version "${Min_Texinfo_VER}"
    fi

    if [ -n "${Min_Xz_VER}" ]; then
        check_xz_version "${Min_Xz_VER}"
    fi

    if [ -n "${Min_Sudo_VER}" ]; then
        check_sudo_version "${Min_Sudo_VER}"
    fi

    if [ -n "${Min_Wget_VER}" ]; then
        check_wget_version "${Min_Wget_VER}"
    fi

    if [ -n "${Min_Dialog_VER}" ]; then
        check_dialog_version "${Min_Dialog_VER}"
    fi
}
