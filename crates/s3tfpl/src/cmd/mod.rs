/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

mod build;
mod version_checker;

pub use build::configure_build;
pub use version_checker::{check_host_versions, HostVersionsError};

/// Enumeration of possible subcommands for the s3tfpl command
#[derive(clap::Subcommand)]
pub enum Command {
    /// Check the software package versions on the host system
    CheckHostVersions {
        /// If the flag is set, the output will be verbose
        #[clap(long, short)]
        verbose: bool,
    },

    /// Configure the building process
    ConfigureBuild,
}
