/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

use anyhow::Result;
use cursive::views::{Dialog, LinearLayout, TextView};

use crate::config::AppSettings;

/// Use the given configuration file as guide to show a TUI to
/// the user that will help them to configure the build process
/// for the Pulsar Linux Stage 3 Toolchain
///
/// # Arguments
///
/// * `config_file` - Path to the configuration file to use
///
/// # Errors
///
/// This function will return an error if the configuration file
/// cannot be read or parsed
///
/// # Examples
///
/// ```
/// use s3tfpl::configure_build;
///
/// let config_file = std::path::PathBuf::from("/home/user/.config/s3tfpl/config.toml");
///
/// configure_build(&config_file).unwrap();
/// ```
pub fn configure_build(cfg: &AppSettings) -> Result<()> {
    // Create the cursive root view
    let mut siv = cursive::default();

    // If the user has a custom theme, use it
    if !cfg.get_theme().is_empty() {
        siv.load_toml(cfg.get_theme()).unwrap();
    }

    // Create the main application layout and add it to the screen
    let layout =
        LinearLayout::vertical().child(TextView::new("Welcome to Pulsar Linux Stage 3 Builder!"));

    siv.add_layer(
        Dialog::around(layout)
            .title("Pulsar Linux Toolchain")
            .button("Quit", cursive::Cursive::quit),
    );

    siv.run();

    Ok(())
}
