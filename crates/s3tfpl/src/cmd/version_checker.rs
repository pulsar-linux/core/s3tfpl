/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

//! This module provides utilities for checking the software package
//! versions on the host system
//!
//! It is used to check if the versions of the packages installed on
//! the host system meet the requirements of the Pulsar Linux Stage 3 Builds

use colored::Colorize;
use thiserror::Error;

use crate::lib::packages::{
    package,
    versions::{self, HOST_PACKAGES},
};

/// Iterates over the `HOST_PACKAGES` data and checks if the requirements are
/// meet on the host system that is running this application
pub fn check_host_versions(verbose: bool) -> Result<(), HostVersionsError> {
    // Check the versions of the packages
    let mut packages = HOST_PACKAGES.iter().cloned().collect::<Vec<_>>();
    check_host_versions_inner(&mut packages, verbose)
}

/// Enumeration of the possible errors that can occur when checking the
/// software package versions on the host system
#[derive(Debug, Error, PartialEq)]
pub enum HostVersionsError {
    /// The package can not be found or is not installed
    /// on the host system
    #[error("The package {0} can not be found or is not installed on the host system")]
    PackageNotFound(String),

    /// The version of the package installed on the host system
    /// is not compatible with the version required by the Pulsar Linux
    /// Stage 3 Builds
    #[error("{0}")]
    PackageVersionMismatch(String),

    /// The command to get the package version failed
    #[error("The command to get the package version failed with error: {0}")]
    CommandError(String),

    /// The command to get the package version returned a non parseable output
    #[error("The command to get the package version returned a non parseable output: {0}")]
    CommandOutputParseError(String),
}

/// Checks if the host system has the required software packages
/// installed and if the versions of the packages installed on the host system
/// are compatible with the versions of the packages required by the Pulsar Linux
/// Stage 3 Builds
///
/// # Arguments
///
/// * `packages` - The list of packages to check
/// * `verbose` - If true, the output will be verbose
///
/// # Returns
///
/// If the versions are compatible, the function returns Ok(())
/// If the versions are not compatible, the function returns `Err(HostVersionsError::PackageVersionMismatch`)
///
/// # Examples
///
/// ```
/// use s3tfpl::lib::packages::{HostVersions, Package};
///
/// let packages = vec![
///    Package::new(
///       "pulsar-stage3-build",
///      "1.0.0",
///     "s3tfpl --version | head -n 1 | cut -d ' ' -f 3",
///   ),
/// ];
///
/// if let Err(error) = check_host_versions_inner(&packages, true) {
///    println!("{}", error);
/// }
/// ```
///
/// # Errors
///
/// This function can return the following errors:
///
/// * `HostVersionsError::PackageNotFound` - The package is not installed on the host system
/// * `HostVersionsError::PackageVersionMismatch` - The version of the package installed on the host system is not compatible with the version required by the Pulsar Linux Stage 3 Builds
/// * `HostVersionsError::CommandError` - The command to get the package version failed
/// * `HostVersionsError::CommandOutputParseError` - The command to get the package version returned an invalid output
///
/// # Notes
///
/// This function returns as soon as one package on the list returns an error
/// during its version check and it does not check the other packages
/// on the list
///
/// # See Also
///
/// * `HostVersionsError`
/// * `Package`
/// * `versions::check_version`
///
pub fn check_host_versions_inner(
    packages: &mut [package::Package],
    verbose: bool,
) -> Result<(), HostVersionsError> {
    // Iterate over the list of packages to check
    for package in packages {
        // Execute the package command to get the package version
        if let Err(err) = package.update_version() {
            if err.contains("127") {
                return Err(HostVersionsError::PackageNotFound(
                    package.name().to_string(),
                ));
            } else if err.contains("Error parsing") {
                return Err(HostVersionsError::CommandOutputParseError(err));
            }

            return Err(HostVersionsError::CommandError(err));
        }

        // Check the package version
        if !versions::check_requirement(package.version(), package.required_version()) {
            return Err(HostVersionsError::PackageVersionMismatch(format!(
                "The version of the package {} installed on the host system ({}) does not meet the required minimum version ({})",
                package.name().bold(), package.version().red(), package.required_version().yellow(),
            )));
        }

        if verbose {
            println!(
                "{:<18}{:<18}{:^24} (Min Version: {:^12})",
                package.name().yellow(),
                package.version().bold(),
                "OK".to_string().green(),
                package.required_version().cyan()
            );
        }
    }

    if verbose {
        println!("\n{}", "software versions in host systems seems compatible with the Pulsar Linux Stage 3 Builds".green());
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_host_versions() {
        let mut packages = vec![package::Package::new("app1", "", "1.0.3", "app1 --version")];

        let r = check_host_versions_inner(&mut packages, true);
        assert!(r.is_err());
        let p = r.unwrap_err();
        assert_eq!(p, HostVersionsError::PackageNotFound("app1".to_string()));

        packages = vec![package::Package::new(
            "bash",
            "",
            ">4.3",
            "bash --version | head -n 1 | cut -d ' ' -f 4",
        )];

        let r = check_host_versions_inner(&mut packages, true);
        assert!(r.is_ok());
    }
}
