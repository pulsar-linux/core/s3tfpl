/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

use std::io::Read;

use colored::Colorize;
use serde::Deserialize;

use crate::lib::{
    shell::expand_tilde,
    themes::{PULSAR_LIGHT_THEME, PULSAR_THEME},
};

/// Enumeration of the different run modes
/// The run mode is used to determine the behavior of the application
#[derive(Debug, Deserialize, Clone)]
pub enum RunMode {
    /// Run in the terminal
    Terminal,
    /// Run in the GUI
    Gui,
}

/// Enumeration of the different themes
/// The theme is used to determine the color scheme of the application
#[derive(Debug, Deserialize, Clone)]
pub enum Theme {
    /// Pulsar theme
    Pulsar,
    /// Pulsar light theme
    PulsarLight,
}

/// Enumeration of the different attendance modes
/// The attendance mode is used to determine the behavior of the application
/// when the user is not present
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub enum AttendanceMode {
    /// User is present
    Attended,
    /// The user is not present
    Unattended,
}

/// Enumeration of the different options for the multi build
/// The multi build option is used to determine which architectures should be built
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub enum MultiBuild {
    /// Build all available architectures
    All,
    /// Build only for amd64
    Amd64,
    /// Build for amd64 and i386
    Amd64AndI386,
    /// Build for amd64, i386 and amd64 with 32bit pointers
    Amd64AndI386AndMx32,
    /// Build for arm64
    Arm64,
}

/// Enumeration of the different options for using CPU cores in the host
/// The cpu cores option is used to determine which CPU cores should be used
/// in the host while compiling the packages
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub enum CpuCores {
    /// Use only one CPU core
    One,
    /// Use half of the CPU cores
    Half,
    /// Use all available CPU cores
    All,
}

/// Enumeration of the different verbosity levels
/// The verbosity level is used to determine the amount of information
/// that should be printed to the terminal
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub enum Verbosity {
    /// Print only errors
    Errors,
    /// Print errors and warnings
    Warnings,
    /// Print errors, warnings and info
    Info,
    /// Print errors, warnings, info and debug
    Debug,
}

/// Data structure that contains the configuration for the application
#[derive(Debug, Deserialize, Clone)]
pub struct AppSettings {
    pub theme: String,
    pub config_file: std::path::PathBuf,
    pub build_path: std::path::PathBuf,
    pub pulsar_version: String,
    pub pgp_key: String,
    pub build_mode: MultiBuild,
    pub attendance_mode: AttendanceMode,
    pub cpu_cores: CpuCores,
    pub verbose: Verbosity,
}

/// Implements `Default` for `AppSettings`
/// This function returns the default configuration for the application
/// The default configuration is:
/// - Theme: `pulsar`
/// - Config file: ~/.config/s3tfpl/config.toml
/// - Build path: ${HOME}/s3tfpl
/// - Pulsar version: latest
/// - PGP key: "" (empty string)
/// - Build mode: `Amd64AndI386`
/// - Attendance mode: `Attended`
/// - CPU cores: `Half`
/// - Verbosity: `Info`  (i.e. print errors, warnings and info)
impl Default for AppSettings {
    fn default() -> Self {
        AppSettings {
            theme: "pulsar".to_string(),
            config_file: expand_tilde("~/.config/s3tfpl/config.toml").into(),
            build_path: std::path::PathBuf::from(
                std::env::var("HOME").unwrap_or_else(|_| "".to_string()) + "/s3tfpl",
            ),
            pulsar_version: "latest".to_string(),
            pgp_key: "".to_string(),
            build_mode: MultiBuild::Amd64AndI386,
            attendance_mode: AttendanceMode::Attended,
            cpu_cores: CpuCores::Half,
            verbose: Verbosity::Info,
        }
    }
}

impl AppSettings {
    /// Returns back the static loaded theme for the application
    /// based in the `theme` field of the configuration
    pub fn get_theme(&self) -> &str {
        match self.theme.as_str() {
            "pulsar_light" => PULSAR_LIGHT_THEME,
            _ => PULSAR_THEME,
        }
    }

    /// Load configuration file from disk
    /// This function loads the configuration file from disk and returns an `AppSettings`
    /// object. If the file does not exist, the default configuration is returned
    ///
    /// # Arguments
    /// * `config_file` - Path to the configuration file
    ///
    /// # Returns
    /// An `AppSettings` object
    ///
    /// # Errors
    /// This function returns an `Err` if the file does not exist
    /// or if the file is not a valid TOML file
    pub fn load(config_file: &std::path::Path) -> Result<Self, std::io::Error> {
        let mut file = std::fs::File::open(expand_tilde(config_file))?;
        let mut buffer = Vec::new();

        file.read_to_end(&mut buffer)?;
        let config: AppSettings = toml::from_slice(&buffer)?;
        Ok(config)
    }
}

#[cfg(test)]
mod tests {
    use std::io::Write;

    use super::*;

    static CONFIG_FILE_DATA: &str = r#"
        theme = "pulsar"
        config_file = "~/.config/s3tfpl/config.toml"
        build_path = "~/s3tfpl"
        pulsar_version = "latest"
        pgp_key = ""
        build_mode = "Amd64AndI386"
        attendance_mode = "Attended"
        cpu_cores = "Half"
        verbose = "Info"
    "#;

    #[test]
    fn test_load_config_file() {
        // write the contents of CONFIG_FILE_DATA to a temporary file
        let mut temp_file = tempfile::NamedTempFile::new().unwrap();
        temp_file.write_all(CONFIG_FILE_DATA.as_bytes()).unwrap();
        temp_file.flush().unwrap();

        // load the configuration file
        let config = AppSettings::load(temp_file.path()).unwrap();
        assert_eq!(config.theme, "pulsar".to_string());
        assert_eq!(
            config.config_file,
            std::path::PathBuf::from("~/.config/s3tfpl/config.toml")
        );
        assert_eq!(config.build_path, std::path::PathBuf::from("~/s3tfpl"));
        assert_eq!(config.pulsar_version, "latest".to_string());
        assert_eq!(config.pgp_key, "".to_string());
        assert_eq!(config.build_mode, MultiBuild::Amd64AndI386);
        assert_eq!(config.attendance_mode, AttendanceMode::Attended);
        assert_eq!(config.cpu_cores, CpuCores::Half);
        assert_eq!(config.verbose, Verbosity::Info);
    }

    #[test]
    fn test_load_settings() {
        let s: AppSettings = toml::from_str(CONFIG_FILE_DATA).unwrap();

        assert_eq!(s.theme, "pulsar");
        assert_eq!(
            s.config_file,
            std::path::PathBuf::from("~/.config/s3tfpl/config.toml")
        );
        assert_eq!(s.build_path, std::path::PathBuf::from("~/s3tfpl"));
        assert_eq!(s.pulsar_version, "latest");
        assert_eq!(s.pgp_key, "");
        assert_eq!(s.build_mode, MultiBuild::Amd64AndI386);
        assert_eq!(s.attendance_mode, AttendanceMode::Attended);
        assert_eq!(s.cpu_cores, CpuCores::Half);
        assert_eq!(s.verbose, Verbosity::Info);
    }
}
