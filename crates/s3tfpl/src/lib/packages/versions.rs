/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

use std::collections::HashMap;

use clap::lazy_static::lazy_static;

use crate::lib::packages::package::Package;

// Host System minimum required software versions
static REQUIRED_PACKAGE_VERSIONS: &str =
    include_str!("../../../../../packages/host_system/required_package_versions.toml");

// Lazy static host system software minimal versions table
lazy_static! {
    pub static ref HOST_PACKAGES: Vec<Package> = {
        let table: HashMap<String, Vec<Package>> =
            toml::from_str(REQUIRED_PACKAGE_VERSIONS).unwrap();
        let packages = table.get("packages").unwrap();

        packages.clone()
    };
}

/// Check whether the given requirement matches the given minimal version
/// using semantic version
///
/// # Arguments
///
/// * `package_version` - The version of the package
/// * `req` - The requirement to check
///
/// # Returns
///
/// `true` if the installed package version is greater than or equal to the given minimal version string
///
/// # Examples
///
/// ```
/// use s3tfpl::lib::packages::versions::check_requirement;
///
/// assert!(check_requirement("1.0.0", ">=1.0.0"));
/// assert!(check_requirement("1.0.0", "1.0.0-alpha.1"));
/// assert!(!check_requirement("1.0.0", "<=1.0.0-alpha.1"));
/// assert!(check_requirement("5.0", ">=4.3, <11.3.0"));
/// assert!(check_requirement("4.8", "^4.3, <11.3.0"));
/// ```
pub fn check_requirement(package_version: &str, req: &str) -> bool {
    // Parse the version requirement string as a semver::VersionReq
    let req = semver::VersionReq::parse(req).unwrap();

    // Try to parse the given package_version as a semver::Version
    // If it fails, create a new semver::Version with the given package_version
    // manually to avoid panicking
    let package_version = if let Ok(v) = semver::Version::parse(package_version) {
        v
    } else {
        let mut split = package_version.split('.');

        semver::Version::new(
            split.next().unwrap_or("0").parse().unwrap_or_default(),
            split.next().unwrap_or("0").parse().unwrap_or_default(),
            split.next().unwrap_or("0").parse().unwrap_or_default(),
        )
    };
    // Check whether the given package_version meets the given requirement
    req.matches(&package_version)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_version_requirement() {
        assert!(check_requirement("1.0.0", ">=1.0.0"));
        assert!(check_requirement("1.0.0", ">1.0.0-alpha.1"));
        assert!(!check_requirement("1.0.0", "<=1.0.0-alpha.1"));
        assert!(check_requirement("5.0", ">=4.3, <11.3.0"));
        assert!(check_requirement("4.8", "^4.3, <11.3.0"));
    }
}
