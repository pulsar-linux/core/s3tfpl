/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

use std::{fmt, process::Command};

use colored::Colorize;
use serde::{Deserialize, Serialize};

/// Package defines a Linux package installed in the host system
/// It is used to store information about the package version
#[derive(Deserialize, Clone, PartialEq, Serialize)]
pub struct Package {
    /// The package name (e.g. "pulsar-stage3-build")
    /// This is the name of the package as it is installed in the host system
    /// It is used to identify the package
    /// It is also used to identify the package in the Pulsar Linux Stage 3 Builds
    name: String,

    /// The package version (e.g. "1.0.0")
    /// This is the version of the package as it is installed in the host system
    version: String,

    /// The package required version (e.g. "1.0.0")
    /// This is the version of the package required by the Pulsar Linux Stage 3 Builds
    required_version: String,

    /// The package command invocation for getting its
    /// version (e.g. "make_pulsar --version | head -n 1 | cut -d ' ' -f 3")
    command: String,
}

impl Package {
    /// Create a new Package
    ///
    /// # Arguments
    ///
    /// * `name` - The package name
    /// * `version` - The package version
    /// * `command` - The package command invocation name
    ///
    /// # Returns
    ///
    /// A new Package
    ///
    /// # Examples
    ///
    /// ```
    /// use s3tfpl::lib::packages::Package;
    ///
    /// let package = Package::new(
    ///     "pulsar-stage3-build",
    ///     "1.0.0",
    ///     "s3tfpl --version | head -n 1 | cut -d ' ' -f 3",
    /// );
    /// ```
    pub fn new(name: &str, version: &str, required_version: &str, command: &str) -> Self {
        Self {
            name: name.to_string(),
            version: version.to_string(),
            required_version: required_version.to_string(),
            command: command.to_string(),
        }
    }

    /// Get the package name
    ///
    /// # Returns
    ///
    /// The package name
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Get the package version
    ///
    /// # Returns
    ///
    /// The package version
    pub fn version(&self) -> &str {
        &self.version
    }

    /// Get the package required version
    ///
    /// # Returns
    ///
    /// The package required version
    pub fn required_version(&self) -> &str {
        &self.required_version
    }

    /// Get the package command invocation name
    ///
    /// # Returns
    ///
    /// The package command invocation
    pub fn command(&self) -> &str {
        &self.command
    }

    /// Execute the package command to get its version and fill
    /// its version field with it
    ///
    /// # Returns
    ///
    /// * `Ok(())` - If the command execution was successful
    /// * `Err(String)` - If the command execution failed
    ///
    /// # Examples
    ///
    /// ```
    /// use s3tfpl::lib::packages::Package;
    /// use std::process::Command;
    ///
    /// let package = Package::new(
    ///     "pulsar-stage3-build",
    ///     "1.0.0",
    ///     "s3tfpl --version | head -n 1 | cut -d ' ' -f 3",
    /// );
    ///
    /// if let Err(e) = package.update_version() {
    ///    panic!("could not get version for package s3tfpl: {}", e);
    /// }
    /// ```
    pub fn update_version(&mut self) -> Result<(), String> {
        // execute POSIX shell command to get the package version
        let output = Command::new("sh")
            .arg("-c")
            .arg(&self.command)
            .output()
            .map_err(|e| format!("{}", e))?;

        if !output.status.success() {
            return Err(format!(
                "Command {} failed with status {}",
                self.command()
                    .split_ascii_whitespace()
                    .next()
                    .unwrap_or("")
                    .bold(),
                output.status.to_string().red()
            ));
        }

        if !output.stderr.is_empty() {
            return Err(format!(
                "Command {} returned error: {}",
                self.command(),
                String::from_utf8(output.stderr).unwrap_or_default().red()
            ));
        }

        let version =
            String::from_utf8(output.stdout).map_err(|e| format!("Error parsing output: {}", e))?;
        self.version = version.trim().to_string();

        Ok(())
    }
}

/*
    Traits implementation
*/

// Implements the `Display` trait for the `Package` struct
impl fmt::Display for Package {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} v{}", self.name, self.version)
    }
}

// Implements the `Debug` trait for the `Package` struct
impl fmt::Debug for Package {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} ({}) v{}", self.name, self.command, self.version)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_package_new() {
        let package = Package::new(
            "pulsar-stage3-build",
            "1.0.0",
            "1.0.0",
            "s3tfpl --version | head -n 1 | cut -d ' ' -f 3",
        );
        assert_eq!(package.name(), "pulsar-stage3-build");
        assert_eq!(package.version(), "1.0.0");
        assert_eq!(package.required_version(), "1.0.0");
        assert_eq!(
            package.command(),
            "s3tfpl --version | head -n 1 | cut -d ' ' -f 3"
        );
    }

    #[test]
    fn test_package_update_version() {
        // create a package with bash and get its version
        let mut package = Package::new(
            "bash",
            "",
            "0.10.2",
            "bash --version  | head -n1 | cut -d' ' -f 4",
        );
        assert_eq!(package.version(), "");
        assert!(package.update_version().is_ok());
        assert!(package.version() != "");

        // create a package that does not exists and get the error
        let mut package = Package::new("does-not-exist", "", "0.10.2", "does-not-exist");
        assert_eq!(package.version(), "");
        let r = package.update_version();
        assert!(r.is_err());
        assert_eq!(
            r.err().unwrap(),
            format!(
                "Command {} failed with status {}",
                "does-not-exist".bold(),
                "exit status: 127".red()
            )
        );
        assert!(package.update_version().is_err());
    }
}
