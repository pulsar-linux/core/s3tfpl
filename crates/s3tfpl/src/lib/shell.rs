/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

use std::{
    borrow::Cow,
    path::{Path, PathBuf},
};

use clap::lazy_static::lazy_static;

/// Expands the home dir tilde `~` to the user's home directory
/// # Arguments
/// * `path` - The path to expand, it can be any object that implements `AsRef<Path>`
///
/// # Returns
/// The expanded path as a `Cow<Path>`
///
/// # Panics
/// Panics if the home directory cannot be found
///
/// # Examples
///
/// ```
/// use s3tfpl::lib::expand_home_dir;
/// let path = expand_home_dir("~/foo/bar");
/// assert_eq!(path, std::env::home_dir().unwrap().join("foo/bar"));
///
/// let path = expand_home_dir("~");
/// assert_eq!(path, std::env::home_dir().unwrap());
///
/// let path = expand_home_dir("/foo/bar");
/// assert_eq!(path, PathBuf::from("/foo/bar"));
///
/// let path = expand_home_dir("");
/// assert_eq!(path, PathBuf::from(""));
///
/// let path = expand_home_dir("~/");
/// assert_eq!(path, std::env::home_dir().unwrap());
/// ```
pub fn expand_tilde<P>(path: &P) -> Cow<'_, Path>
where
    P: AsRef<Path> + ?Sized,
{
    let path = path.as_ref();

    if !path.starts_with("~") {
        return path.into();
    }

    lazy_static! {
        static ref BUF: PathBuf = PathBuf::from(std::env::var_os("HOME").unwrap());
        static ref HOME_DIR: &'static Path = BUF.as_ref();
    }

    HOME_DIR.join(path.strip_prefix("~").unwrap()).into()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_expand_tilde() {
        let home = PathBuf::from(std::env::var("HOME").unwrap());

        assert_eq!(expand_tilde("~/foo/bar"), home.join("foo/bar"));
        assert_eq!(expand_tilde("~"), home);
        assert_eq!(expand_tilde("/foo/bar"), PathBuf::from("/foo/bar"));
        assert_eq!(expand_tilde(""), PathBuf::from(""));
        assert_eq!(expand_tilde("~/"), home);

        let path_buf = PathBuf::from("~/foo/bar");
        assert_eq!(expand_tilde(&path_buf), home.join("foo/bar"));

        let p_path = Path::new("~/foo/bar");
        assert_eq!(expand_tilde(p_path), home.join("foo/bar"));
    }
}
