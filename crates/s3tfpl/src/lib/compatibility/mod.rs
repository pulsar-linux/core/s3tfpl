/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

//! This module contains compatibility functions and utilities

use std::process::Command;

use colored::Colorize;
use thiserror::Error;

/// Enumeration of the possible errors that can occur when checking the
/// compatibility of the system with the chosen configuration
#[derive(Debug, Error, PartialEq)]
pub enum CompatError {
    /// The host system can not create x64_86 binaries
    #[error("The host system can not create x64_86 binaries")]
    X64BinaryNotSupported,

    /// The host system can not create x86 binaries
    #[error("The host system can not create x86 binaries")]
    X86BinaryNotSupported,

    /// The host system can not create x32 ABI for 64-bit mode binaries
    #[error("The host system can not create x32 ABI for 64-bit mode binaries")]
    X32BinaryNotSupported,

    /// The host system can not create aarch64 binaries
    #[error("The host system can not create aarch64 binaries")]
    Aarch64BinaryNotSupported,

    /// The host system can not create arm binaries
    #[error("The host system can not create arm binaries")]
    ArmBinaryNotSupported,

    /// The given architecture is not sopported or recognized
    #[error("The given architecture is not sopported or recognized")]
    UnsupportedArch,
}

/// Checks if the host system can produce x64_86 binaries
///
/// # Arguments
///
/// * `verbose` - If true, the output will be verbose
///
/// # Returns
///
/// * `Ok(())` - If the host system can produce x64_86 binaries
/// * `Err(())` - If the host system can not produce x64_86 binaries
///
/// # Errors
///
/// * `CompatError::X64BinaryNotSupported` - If the host system can not produce x64_86 binaries
///
/// # Example
/// ```
/// use s3tfpl::compat::check_compat;
///
/// let result = check_compat_x64(true);
/// assert!(result.is_ok());
/// ```
pub fn check_compat_x64(verbose: bool) -> Result<(), CompatError> {
    check_compat("m64", verbose)
}

/// Check if the host system can produce x86 binaries (32-bit mode)
///
/// # Arguments
///
/// * `verbose` - If true, the output will be verbose
///
/// # Returns
///
/// * `Ok(())` - If the host system can produce x86 binaries
/// * `Err(())` - If the host system can not produce x86 binaries
///
/// # Errors
///
/// * `CompatError::X86BinaryNotSupported` - If the host system can not produce x86 binaries
///
/// # Example
///
/// ```
/// use s3tfpl::compat::check_compat;
///
/// let result = check_compat_x86(true);
/// assert!(result.is_ok());
/// ```
pub fn check_compat_x86(verbose: bool) -> Result<(), CompatError> {
    check_compat("m32", verbose)
}

/// Check if the host system can produce x32 ABI binaries for 64-bit mode binaries
///
/// # Arguments
///
/// * `verbose` - If true, the output will be verbose
///
/// # Returns
///
/// * `Ok(())` - If the host system can produce x32 ABI binaries for 64-bit mode binaries
/// * `Err(())` - If the host system can not produce x32 ABI binaries for 64-bit mode binaries
///
/// # Errors
///
/// * `CompatError::X32BinaryNotSupported` - If the host system can not produce x32 ABI binaries for 64-bit mode binaries
///
/// # Example
///
/// ```
/// use s3tfpl::compat::check_compat;
///
/// let result = check_compat_x32(true);
/// assert!(result.is_ok());
/// ```
pub fn check_compat_x32(verbose: bool) -> Result<(), CompatError> {
    check_compat("mx32", verbose)
}

/// Check if the host system can produce arm binaries (32-bit mode)
///
/// # Arguments
///
/// * `verbose` - If true, the output will be verbose
///
/// # Returns
///
/// * `Ok(())` - If the host system can produce arm binaries (32-bit mode)
/// * `Err(())` - If the host system can not produce arm binaries (32-bit mode)
///
/// # Errors
///
/// * `CompatError::ArmBinaryNotSupported` - If the host system can not produce arm binaries (32-bit mode)
///
/// # Example
///
/// ```
/// use s3tfpl::compat::check_compat;
///
/// let result = check_compat_arm(true);
/// assert!(result.is_ok());
/// ```
fn check_compat_arm(verbose: bool) -> Result<(), CompatError> {
    // Run the check command
    if let Err(e) = check_command("echo \"int main() {}\" | arm-none-eabi-gcc -x c -specs=/usr/arm-none-eabi/lib/aprofile-ve.specs -o /dev/null -") {
        if verbose {
            eprintln!("{}", e);
        }
        Err(CompatError::ArmBinaryNotSupported)
    } else {
        if verbose {
            println!("{}", "The host system can produce arm binaries (32-bit mode)".green());
        }
        Ok(())
    }
}

/// Check if the host system can produce aarch64 binaries (64-bit mode)
///
/// # Arguments
///
/// * `verbose` - If true, the output will be verbose
///
/// # Returns
///
/// * `Ok(())` - If the host system can produce aarch64 binaries (64-bit mode)
/// * `Err(())` - If the host system can not produce aarch64 binaries (64-bit mode)
///
/// # Errors
///
/// * `CompatError::Aarch64BinaryNotSupported` - If the host system can not produce aarch64 binaries (64-bit mode)
///
/// # Example
///
/// ```
/// use s3tfpl::compat::check_compat;
///
/// let result = check_compat_aarch64(true);
/// assert!(result.is_ok());
/// ```
fn check_compat_aarch64(verbose: bool) -> Result<(), CompatError> {
    // Run the check command
    if let Err(e) =
        check_command("echo \"int main() {}\" | aarch64-linux-gnu-gcc -x c -o /dev/null -")
    {
        if verbose {
            eprintln!("{}", e);
        }
        Err(CompatError::Aarch64BinaryNotSupported)
    } else {
        if verbose {
            println!(
                "{}",
                "The host system can produce aarch64 binaries (64-bit mode)".green()
            );
        }
        Ok(())
    }
}

// Convenience function to DRY the code
fn check_compat(arch: &str, verbose: bool) -> Result<(), CompatError> {
    // Run the check_command with the given command string
    if let Err(e) = check_command(&format!(
        "echo \"int main() {{}}\" | gcc -{} -x c -o /dev/null -",
        arch
    )) {
        if verbose {
            eprintln!("{}", e.red());
        }
        match arch {
            "m64" => Err(CompatError::X64BinaryNotSupported),
            "m32" => Err(CompatError::X86BinaryNotSupported),
            "mx32" => Err(CompatError::X32BinaryNotSupported),
            _ => Err(CompatError::UnsupportedArch),
        }
    } else {
        if verbose {
            println!(
                "{}",
                format!("The host system can produce {} binaries", arch).green()
            );
        }
        Ok(())
    }
}

// Executes the given command and returns Ok(()) if the command returns 0
// and Err(()) if the command returns a non-zero exit code
fn check_command(command: &str) -> Result<(), String> {
    let output = Command::new("sh")
        .arg("-c")
        .arg(command)
        .output()
        .expect("Failed to execute command");

    if output.status.success() {
        Ok(())
    } else {
        Err(String::from_utf8_lossy(&output.stderr).to_string())
    }
}
