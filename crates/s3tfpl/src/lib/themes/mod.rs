/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

/// Pulsar default theme
pub static PULSAR_THEME: &str = include_str!("pulsar.toml");

/// Pulsar light theme
pub static PULSAR_LIGHT_THEME: &str = include_str!("pulsar_light.toml");
