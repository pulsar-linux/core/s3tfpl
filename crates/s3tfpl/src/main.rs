/*
    This file is part of s3tfpl utility, see LICENSE file for details
    Copyright (C) 2022 - The Pulsar Linux Team
*/

//! This crate provides a CLI/TUI for Pulsar Linux
//! Stage 3 Builds

#![deny(
    nonstandard_style,
    rust_2018_idioms,
    rust_2021_compatibility,
    missing_docs
)]
#![warn(clippy::all, clippy::pedantic)]

mod cmd;
mod config;
mod lib;

use anyhow::{Context, Result};
use clap::Parser;

use cmd::{check_host_versions, configure_build, Command};

/// Pulsar Linux Stage 3 Toolchain Command Line Interface
#[derive(Parser)]
struct Cli {
    /// Path to the configuration file
    #[clap(
        short,
        long,
        default_value = "~/.config/s3tfpl/config.toml",
        parse(from_os_str)
    )]
    config_file: std::path::PathBuf,

    #[clap(subcommand)]
    command: Command,
}

// This is the main function
fn main() -> Result<()> {
    // Parse the command line arguments
    let args = Cli::parse();

    // Load configuration from file
    let cfg = config::AppSettings::load(&args.config_file)?;

    match args.command {
        Command::CheckHostVersions { verbose } => {
            // Check the software package versions on the host system
            check_host_versions(verbose).context("unable to validate host system versions")
        }
        Command::ConfigureBuild => configure_build(&cfg).context("unable to configure build"),
    }
}
