<!---
    README Title and short description
-->
# Stage 3 Toolchain for Pulsar Linux

This repository contains a toolchain to prepare a stage 3 tarball and a Kernel as a base to build a full Pulsar Linux distribution.

# Motivation

Automating the creation of Pulsar's stage 3 minimal environment makes possible to iterate faster and take advantage of CI/CD pipelining support, explicitly, Gitlab CI/CD. The aim for this project is to completely automate the creation of Pulsar's stage 3 and prepare a pipeline able to reconstruct itself on demand with new versions of upstream software if required.

<!--
    Disclaimers and Notices
-->

# Disclaimer

This repository takes a lot of inspiration from the great ALFS project by Jeremy Huntwork, Thomas Pegg, Pierre Labastie and others. You can check their work in the [ALFS project][ALFS] site.

# Notices

Pulsar's Stage 3 contains the "minimal" system required to construct a Pulsar Linux distribution, this means, it already provides of Pulsar's specific practices and decisions over the directories hierarchy.

## Binaries location

Pulsar maintains regular user binaries and system binaries separated. Regular user binaries resides in `/bin` while system binaries reside in `/sbin`.

## User System Resources merge

Pulsar merges the directories stated below into the **User System Resources** (/usr) directory:

* /bin
* /sbin
* /lib
* /lib64

What means that `/bin` is a symbolic link to `/usr/bin`, `/sbin` to `/usr/sbin` and so on. This makes software packaging easier as there is only one installation tree.

# Conventions and Nomenclature

This document follows some name conventions and nomenclature

| literal | meaning |
| ------- | ------- |
| target system/target | the minimal GNU/Linux system needed to compile a full Pulsar Linux distribution |
| host system/host | an already fully working GNU/Linux system where we build the **target system** |
| $PULSAR | a globally exported variable containing the path to the directory where our **target system** will be compiled and installed |
| the chroot/chroot | the special environment created by the [chroot][chroot_command] command inside our **target system** |
| stage | an specific building phase of the s3tflp minimal **target system** <sub>[pulsar stages][stages]</sub>

# Prerequisites

You will need some software in order to be able to execute this toolchain and build a minimal base system for Pulsar Linux, you must install the software listed below if it is not already installed in the host machine

| dependency | usage |
| ---------- | ----- |
| bash | used to execute commands and shell scripts |
| binutils >= 2.13.1 | provides common utilities used when working with binary files |
| bison >= 2.7 | bison is used by many GNU tools, it provides of a replacement for `yacc` |
| coreutils >= 6.9| provides many of a typical Linux commands, including `chroot` |
| gawk >- 4.0.1 | GNU version of the `awk` language and tool |
| gcc >= 4.8 | the GNU C and C++ compiler required to compile anything |
| diffutils >= 2.8.1 | used to create and manipulate patch files |
| findutils >= 4.2.31 | provides the `find` and `xargs` commands |
| grep >= 2.5 | provides the `grep` command |
| gzip >= 1.3.12 | GNU compression tools, used to decompress compressed tarballs |
| Linux Kernel >= 4.4.0 | the Linux Kernel version 4.4.0 or higher (this is needed while compiling glibc)|
| m4 >= 1.4.10 | the GNU macro processor, used to compile majority of software |
| make >= 4.0 | the GNU make utility, used to orchestrate the compilation of complex binaries |
| patch >= 2.5.4 | used to apply diff files as patches |
| wget | used to download software from the Internet |
| sudo (or doas) | used to execute commands as the super user |

# Installation

This project does not requires any installation, just clone the repository and execute the `make_pulsar` script as root, then, follow the instructions.

> **warning**: users must take precautions while using this tool chain, a wrongly configured environment could potentially lead to the user's host system utterly and irremissible destruction, we don't make ourselves responsible of the consequences of an inadequate use of this software can have in your system

# Preparing the build System

If building s3tfpl manually, the recommendation is to create a partition to host the new PULSAR system stage 3 data. Of course, one can build the new PULSAR system in the `root` partition or any other but problems might arise if issues appear during the build process. Having the system to be compiled in its own partition allow us to completely destroy it and recreate if we need to without putting in danger the integrity of the host system.

## Preparing the $PULSAR partition

You can use any block device of your choice as a target for the `$PULSAR` target system (even a directory in an already existent partition, but not using a dedicated partition is highly discouraged). The following block device handle locations are the most commonly present in any modern hardware (where {n} will be the device number or letter in case of SATA type devices)

| Device type | Handle location |
| - | - |
| SATA, SAS, SCSI and USB | /dev/sd{n} |
| NVM Express | /dev/nvme{n}n1 |
| MMC, eMMC and SD | /dev/mmcblk{n} |

After choosing the right block device for you, you can create a new partition using the `fdisk` tool.

> We are assuming that users know how to properly partition their hard drives so no specific instructions are provided in this document, if disk partitioning is an alien topic for you, we recommend you stop right now, read [The Linux Documentation Project][tldp_partition] partition section (or the whole book) and come back after you are comfortable with manual disk partitioning

> **note**: this is not a tutorial or a manual about how to make a Linux distribution, if you want to learn how to make a Linux distribution from scratch we recommend you to follow the [Linux From Scratch][lfs_book] book

## Formatting the $PULSAR partition

You can format your newly created partition with any file format that you like, it is not important for the goal of this project so you can use whatever that floats your boat. We recommend to stay in the safe path and just use `ext4`

## Mounting the $PULSAR partition

You can mount the `$PULSAR` partition wherever your hearth desires but the toolchain expects it to be mounted in `/mnt/pulsar_build` by default, you can change that path location using the configuration tool later if you like

> Do not be confused, to achieve this project goal (creating a stage3 pulsar tarball) no additional partitions or swap space is required, we are not "installing" pulsar, we are just building a minimal system

# Usage

After cloning this repository in a location of your choice (it is not important) cd into it and run the commands below

```shell
chmod 755 ./make_pulsar
./make_pulsar
```

Follow the instructions shown in the UI that will be presented to you

# Unattended Usage

TBD

# Appendix

More in detail information about specific terms or topics that are not really important to fully understand in order to work with s3tfpl toolchain. Use it as a reference.

## Stages

Pulsar Linux minimal target system building consists of three different stages (or milestones) that need to happen in order. The following is an over simplification of that process.

> Cross compiling is a complex topic, to know more about it you can start with the [Cross Compiler][wiki_cc] wikipedia's article or the [Cross-Compiler Linux From Scratch][cclfs] book online

### Stage 1

During stage one, we build a cross compiling toolchain using the compiler installed in the **host system**.
This cross compiling toolchain will be used to compile software that will target the **$PULSAR** target.
On this way we decouple any software compiled by the cross compiler from the **host system** used to build the stage 1

### Stage 2

On stage two, we use the cross compiler built in step one to build the compiler and standard C and (degraded) C++ library that will finally be used to compile the final binaries that will be installed in the **target host** in stage three

### Stage 3

In stage three, we use the compiler (and standard library) that was built on stage 2 inside a **chroot** to finally compile the binaries for our target system for both x86 and amd64 architectures

# Contributing

TBD

# License

Pulsar is licensed under the terms of the "GNU Public License v3 or any later version", see the [LICENSE][GPLv3] file for detailed information

# Authors

Refer to the [AUTHORS][authors] file for more information

<!---
    Links
-->

[ALFS]: https://linuxfromscratch.org/alfs/index.html
[GPLv3]: https://gitlab.com/pulsar-linux/core/s3tfpl/-/blob/main/LICENSE
[AUTHORS]: https://gitlab.com/pulsar-linux/core/s3tfpl/-/blob/main/AUTHORS
[chroot_command]: https://en.wikipedia.org/wiki/Chroot
[wiki_cc]: https://en.wikipedia.org/wiki/Cross_compiler
[cclfs]: https://www.clfs.org/view/CLFS-3.0.0-SYSVINIT/x86_64/
[tldp_partition]: https://tldp.org/HOWTO/Partition/
[lfs_book]: https://www.linuxfromscratch.org/

[stages]: #stages
